package com.example.service1.mvc;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://service2-chart.test:8081", name="service2-chart")
public interface Service2Client {

    @GetMapping("/service2")
    String invokeService2();

}
