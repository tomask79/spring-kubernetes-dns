# Kubernetes and service discovery through DNS #

In the [previous repository](http://bitbucket.org/tomask79/spring-cloud-kubernetes-ribbon/) I was testing how you can discover and invoke another service via iteraction with [Kubernetes API](https://kubernetes.io/docs/reference/using-api/api-overview/).
With that approach to discover the URL of the K8s service all you needed to know is just the name. But you have to give your application 
appropriate [role](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) to do so. Anyway there is another solution, discovery through DNS.    

The documentation says:

> Every Service defined in the cluster (including the DNS server itself) is assigned a DNS name. 
> By default, a client Pod�s DNS search list will include the Pod�s own namespace and the cluster�s default domain. 
> This is best illustrated by example:
> Assume a Service named foo in the Kubernetes namespace bar. 
> A Pod running in namespace bar can look up this service by simply doing a DNS query for foo. 
> A Pod running in namespace quux can look up this service by doing a DNS query for foo.bar.

Cool, let's try that!

## Kubernetes service invocation, services in different namespaces ##

We're going to prepare two kubernetes services. 

* service1-chart, entrypoint service with [NodePort type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) in the default namespace.
* service2-chart, two replicas clusterIP service we're going to invoke from service1-chart. 

**service2-chart**

Again it's going to be simple MVC controller:

```java
@RestController
public class Controller {

    @GetMapping(path = "/service2")
    public String respondPoint() {
        return "Hello I'm service2";
    }
}
```
resources:

```
spring.application.name=service2-chart
server.port=8081
```
and again I prepared easy Helm chart to install everything, but first things first:

* tomask79:spring-service2 tomask79$ eval $(minikube docker-env)
* tomask79:spring-service2 tomask79$ mvn clean install

now package prepared helm chart and **install it into "test" namespace**:

    tomask79:spring-service2 tomask79$ helm package ./service2-chart --debug
    Successfully packaged chart and saved it to: /Users/tomask79/workspace/headless/master/spring-service2/service2-chart-0.1.0.tgz
    [debug] Successfully saved /Users/tomask79/workspace/headless/master/spring-service2/service2-chart-0.1.0.tgz to /Users/tomask79/.helm/repository/local
    tomask79:spring-service2 tomask79$ helm install -n service2-chart --namespace test service2-chart-0.1.0.tgz 

expected output, two pods because I set two replicas in a chart:

    tomask79:spring-service2 tomask79$ kubectl -n test get pods
    NAME                              READY     STATUS    RESTARTS   AGE
    service2-chart-6c8698749d-4jg8b   1/1       Running   0          37s
    service2-chart-6c8698749d-xnmp4   1/1       Running   0          37s

and services in the test namespace:

    tomask79:spring-service2 tomask79$ kubectl -n test get services
    NAME             TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
    service2-chart   ClusterIP   10.99.224.69   <none>        8081/TCP   1m


**service1-chart**

Okay, there is an **service2-chart service** in the **test** namespace.    
To invoke it from the default namespace, FeignClient will look like:    

```java
@FeignClient(url = "http://service2-chart.test:8081", name="service2-chart")
public interface Service2Client {

    @GetMapping("/service2")
    String invokeService2();

}
```
To understand why the URL looks like this, read again the documentation part above.    
Syntax is: <service-name>.<namespace>. Now MVC Rest client of service1-chart will use this stub as:

```java
@RestController
public class Controller {

    @Autowired
    private Service2Client service2Client;

    @GetMapping(path = "/service1")
    public String respondPoint() {
        final String service2Output = service2Client.invokeService2();
        return "Hello I'm Service1, output from service2: "+service2Output;
    }
}
```
Everything ready, let's pack it and test it!

    tomask79:spring-service1 tomask79$ mvn clean install
    tomask79:spring-service1 tomask79$ helm package ./service1-chart --debug
    Successfully packaged chart and saved it to: /Users/tomask79/workspace/headless/master/spring-service1/service1-chart-0.1.0.tgz
    [debug] Successfully saved /Users/tomask79/workspace/headless/master/spring-service1/service1-chart-0.1.0.tgz to /Users/tomask79/.helm/repository/local
    tomask79:spring-service1 tomask79$ helm install -n service1-chart service1-chart-0.1.0.tgz 

this is the final state before the test (notice the namespaces):

    tomask79:spring-service1 tomask79$ helm list
    NAME          	REVISION	UPDATED                 	STATUS  	CHART               	APP VERSION	NAMESPACE
    service1-chart	1       	Sun Jan  6 22:24:53 2019	DEPLOYED	service1-chart-0.1.0	1.0        	default  
    service2-chart	1       	Sun Jan  6 22:10:13 2019	DEPLOYED	service2-chart-0.1.0	1.0        	test     

So let's try whether service1-chart service in the default namespace can invoke service2-chart service    
in the test namespace.

    tomask79:spring-service1 tomask79$ minikube service service1-chart --url
    http://192.168.99.100:32229
    tomask79:spring-service1 tomask79$ curl http://192.168.99.100:32229/service1
    Hello I'm Service1, output from service2: Hello I'm service2

Now since service2-chart service **runs in two replicas** then requests will be loadbalanced.
    
## Kubernetes service invocation, services in the same namespaces ##

If you've got your services in the same namespace then there is no need for the namespace suffix in the invocation URL.    
In my cause of service1-chart and service2-chart services the only change would be in the Service2Client stub:

```java
    @FeignClient(url = "http://service2-chart:8081", name="service2-chart")
    public interface Service2Client {

        @GetMapping("/service2")
        String invokeService2();
    }
```
    
everything else stays the same.

## Summary ##

So when discovering and invoking the services in the K8s cluster you've got two options. Either to ask    
**Kubernetes master** where the service is running or use the mentioned **DNS mechanism** to get the target URL.

regards

Tomas